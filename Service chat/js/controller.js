var app = angular.module('serviceChat', ['ionic', 'tabSlideBox'])
    .run(['$q', '$http', '$rootScope', '$location', '$window', '$timeout',
        function ($q, $http, $rootScope, $location, $window, $timeout) {
            $rootScope.$on("$locationChangeStart", function (event, next, current) {
                $rootScope.error = null;
                console.log("Route change!!!", $location.path());
                var path = $location.path();
                console.log("App Loaded!!!");
            });
        }
    ]);

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('index', {
        url: '/',
        templateUrl: 'index.html',
        controller: 'IndexCtrl'
    });




    $stateProvider.state('chatmng', {
        url: '/chatmng',
        templateUrl: 'chatmng.html',
        controller: 'chatmngCtrl'
    });



    $stateProvider.state('addtag', {
        url: '/addtag',
        templateUrl: 'addtag.html',
        controller: 'tagCtrl'
    });

    $urlRouterProvider.otherwise("/");
});

app.controller("IndexCtrl", ['$rootScope', "$scope", "$ionicPopover", "$stateParams", "$q", "$location", "$window", '$timeout',
    function ($rootScope, $scope, $ionicPopover, $stateParams, $q, $location, $window, $timeout, BackButton) {
        $scope.platform = ionic.Platform.platform();
        $scope.yesterday = false;
        $scope.feedbackPending = false;

        $scope.goBack = function () {
            doPageBack();
        }
        $scope.goHome = function () {
            doPageHome();
        }

        $scope.active = function () {
            $window.location.href = '#/';
        }

        $timeout(function () {
            iOSContextCreated();
        }, 50);

        $timeout(function () {
            hideQviki();
            /* var data = getCaseDetailsOfHall("0","1");
      $scope.activeOrders = JSON.parse(data);
*/
            var filter = "case";
            /*var date = "1485243937000|1485236737000";*/
            var date = "";
            var semi = "status|1";
            var orderby = "desc";
            var isshare = 1;
            var data = getAllObjectData(filter, 0, 0, orderby, date, semi, "1"); /*(filter,skip,limit,orderBy,daterange,samifilter,isshare) */
            // doMessageToast(data);     
            $scope.activeOrders = JSON.parse(data);
        }, 200);

        $scope.loadComleteOrders = function () {
            /* var dataCompleteOrder = getCaseDetailsOfHall("0","0");
             $scope.completeOrders = JSON.parse(dataCompleteOrder);*/

            var filter = "case";
            var date = "";
            var semi = "status|0";
            var orderby = "desc";
            var isshare = 1;
            var dataCompleteOrder = getAllObjectData(filter, 0, 0, orderby, date, semi, "1");
            $scope.completeOrders = JSON.parse(dataCompleteOrder);
            for (var j = 0; j < $scope.completeOrders.length; j++) {
                if ($scope.completeOrders[j].data.orderstatus == 1) {
                    $scope.feedbackPending = true;
                } else {
                    $scope.feedbackPending = false;
                }
            }
        }

        $scope.swipe = function () {
            doMessageToast("swipe call");
        }


        $scope.get_Tag = function (data) {

            var count = 0;
            var titleName = '';
            for (var j = 0; j < data.orderitem.length; j++) {
                titleName = data.orderitem[0].quantity + " " + data.orderitem[0].itemname;
                count++;
            }
            if (count == 1) {
                return titleName;
            } else {
                return titleName + " +" + (count - 1);
            }
        }

        $scope.getBill = function (data) {
            var bill = 0;
            for (var j = 0; j < data.orderitem.length; j++) {
                bill += data.orderitem[j].quantity * data.orderitem[j].itemprice;
            }
            return bill;
        }

        $scope.loadPageNg = function (caseIdv, isComplete) {
            hello(caseIdv, isComplete);
            $window.location.href = '#/chatmng';
        }
        $scope.loadchat = function (caseid, receivers) {
            window.localStorage['caseid'] = caseid;
            window.localStorage['receivers'] = receivers;
            $window.location.href = '#/chatmng';
        }

        $scope.userLogo = function (mxID) {
            var visitorData = getVisitorInfo(mxID);

            if (visitorData == null || visitorData == "null") {
                return ["user.jpg", "Walk in"];
            } else {
                var visitorObj = JSON.parse(visitorData);
                if (visitorObj.profilepic == null || visitorObj.profilepic == "") {

                    if (visitorObj.visitorcontext == null) {
                        var returnVisitorData = ["user.jpg", "Walk in"];
                    } else {
                        var returnVisitorData = ["user.jpg", visitorObj.visitorcontext.roomnumber];
                    }


                    return returnVisitorData;
                } else {
                    var returnVisitorData;

                    if (visitorObj.visitorcontext == "" || visitorObj.visitorcontext == null) {
                        //showToastPlateform(visitorObj.visitorcontext);
                        var returnVisitorData = [visitorObj.profilepic, "Walk in"];
                    } else {
                        var returnVisitorData = [visitorObj.profilepic, visitorObj.visitorcontext.roomnumber];
                    }
                    return returnVisitorData;
                }
            }
        }
        $scope.orderTime = function (date) {
            return getDifferenceFromCurrentTime(date);
        }
        $scope.getCaseCount = function (caseid) {
            return getCaseCount(caseid);
        }

        $scope.epocDays = function (date) {
            var time = 0;
            var d = new Date();
            var today = d.getTime();
            var millisBetween = today - parseInt(date);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisecondsPerHour = 1000 * 60 * 60;
            var millisecondsPerMinute = 1000 * 60 * 60;
            var millisecondsPerSecond = 1000 * 60;
            var days = millisBetween / millisecondsPerDay;
            var hour = millisBetween / millisecondsPerHour;
            var minuts = millisBetween / millisecondsPerMinute;
            var second = millisBetween / millisecondsPerSecond;

            if (Math.floor(days) == 0) {
                time = "Today";
            } else {
                if (Math.floor(days) == 1) {
                    time = "Yesterday";
                } else {
                    time = Math.floor(days) + " day ago";
                }
            }
            return time;
        }


        $scope.todayOrderis = function (date) {

            var time = 0;
            var d = new Date();
            var today = d.getTime();
            var millisBetween = today - parseInt(date);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisecondsPerHour = 1000 * 60 * 60;
            var millisecondsPerMinute = 1000 * 60 * 60;
            var millisecondsPerSecond = 1000 * 60;
            var days = millisBetween / millisecondsPerDay;
            var hour = millisBetween / millisecondsPerHour;
            var minuts = millisBetween / millisecondsPerMinute;
            var second = millisBetween / millisecondsPerSecond;

            if (Math.floor(days) == 0) {
                return true;
            } else {
                return false;
            }
        }

        $scope.todayOrderNot = function (date) {
            //showToastPlateform(date);
            var time = 0;
            var d = new Date();
            var today = d.getTime();
            var millisBetween = today - parseInt(date);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisecondsPerHour = 1000 * 60 * 60;
            var millisecondsPerMinute = 1000 * 60 * 60;
            var millisecondsPerSecond = 1000 * 60;
            var days = millisBetween / millisecondsPerDay;
            var hour = millisBetween / millisecondsPerHour;
            var minuts = millisBetween / millisecondsPerMinute;
            var second = millisBetween / millisecondsPerSecond;

            if (Math.floor(days) == 0) {
                return false;
            } else {
                return true;
            }
        }
    }
])

app.controller("chatmngCtrl", ['$rootScope', "$scope", "$ionicPopover", "$ionicModal", "$stateParams", "$q", "$location", "$window", '$timeout', '$ionicScrollDelegate', '$ionicPopup', 'menuShare',
    function ($rootScope, $scope, $ionicPopover, $ionicModal, $stateParams, $q, $location, $window, $timeout, $ionicScrollDelegate, $ionicPopup, menuShare) {
        $scope.platform = ionic.Platform.platform();
        $scope.focusedHide = true;
        $scope.chatmsg = '';
        $scope.name = '';
        $scope.mobile = '';
        $scope.uname = 'Title';
        $scope.uroom = 'Walk in';
        $scope.casedata = '';
        $scope.isComplete = 0;
        $scope.visitorid = '';

        $scope.$on('$ionicView.loaded', loadView);

        function loadView() {
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('homeScroll')
                    .scrollBottom();
                showQviki();
            }, 0);
        }
        $scope.loadViewBottom = function () {
            $ionicScrollDelegate.$getByHandle('homeScroll')
                .scrollBottom();
        }
        $scope.goBack = function () {
            doPageBack();
        }
        $scope.goHome = function () {
            doPageHome();
        }
        $scope.indexPage = function () {
            $window.location.reload();
        }

        $scope.active = function () {
            $window.location.href = 'index.html';
        }

        $timeout(function () {

            var filter = localStorage.getItem("caseid");
            var receivers = localStorage.getItem("receivers");
            // doMessageToast(filter); doMessageToast(receivers);
            // getCaseId();
            var date = "";
            var semi = "";
            var orderby = "asc";
            var isshare = 1;
            var data = getAllObjectData(filter, 0, 0, orderby, semi, date, "1"); /*(filter,skip,limit,orderBy,daterange,samifilter,isshare) */
            // doMessageToast(data);
            chatView(data);
            var dd = getUserDetail();
            //doMessageToast(dd);
            userObjectJS = JSON.parse(dd);
            $scope.mobile = userObjectJS[0].mobile;
            $scope.name = userObjectJS[0].firstName;
            $scope.visitorid = userObjectJS[0].visitorid;

        }, 10);
        $timeout(function () {
            $scope.isComplete = document.getElementById("isComplete")
                .value;
            /* var getdata = getObjectData(localStorage.getItem("caseid"));
             var caseData = JSON.parse(getdata);
             $scope.casedata = caseData[0].orderitem;
             $scope.uname = caseData[0].casename;
             $scope.uroom = room;*/

            if ($scope.isComplete == 1) {
                hideQviki();
            }

        }, 500);
        $scope.setFeedback = function () {

            $scope.popover.hide();
            /*var caseDataObj = menuShare.getData();*/
            // doMessageToast("caseDataObj"+caseDataObj);
            setFeedback();
        }
        $scope.setTag = function () {

            $scope.popover.hide();
            document.location.href = "#/addtag";
        }
        $scope.sendChatData = function () {
            var message = document.getElementById("chatmsg")
                .value;
            if (message != '') {
                var messageJson = '{"message":"' + message + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + formatAMPM() + '","type":"msg","createTime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '" }';

                var ret = setObjectData(messageJson, localStorage.getItem("caseid"), localStorage.getItem("receivers"));
                document.getElementById("chatmsg")
                    .value = '';
            }
        }
        $scope.updateCustomRequest = function (data) {
            $scope.chatData = JSON.parse(data);
        }
        /*$scope.updateCustomRequest1 = function (userObject) {
          userObjectJS=JSON.parse(userObject);
          $scope.name=userObjectJS.name;
          $scope.mobile=userObjectJS.mobile;
        }*/
        $scope.canceledOrder = function () {

            var message = "Your Order has been canceled";
            var getdata = getObjectData(localStorage.getItem("caseid"));
            var caseData = JSON.parse(getdata);
            var filter = "case";
            var data = '{"casename":"' + caseData[0].casename + '","caseowner":"' + caseData[0].caseowner + '","mobile":"' + caseData[0].mobile + '","createtime":"' + epocTime() + '","receivers":"' + caseData[0].receivers + '","status":"0","orderitem":"' + JSON.stringify(caseData[0].orderitem) + '","orderstatus":"' + caseData[0].orderstatus + '","orderduration":"' + caseData[0].orderduration + '","orderdatetime":"' + caseData[0].orderdatetime + '"}';
            //document.write(data); exit;
            //doMessageToast(data);
            var updata = updateObjectData(localStorage.getItem("caseid"), data);
            doMessageToast(updata);
            var messageJson = '{"message":"' + message + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + formatAMPM() + '","type":"msg","createTime":"' + epocTime() + '","receivers":"' + caseData[0].receivers + '" }';

            var ret = setObjectData(messageJson, localStorage.getItem("caseid"), caseData[0].receivers);

            $scope.popover.hide();

        }
        $scope.response = function (msg) {

            var messageJson = '{"message":"' + msg + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + formatAMPM() + '","type":"msg","createTime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '" }';

            var ret = setObjectData(messageJson, localStorage.getItem("caseid"), localStorage.getItem("receivers"));
            $scope.modal.hide();
            $scope.popover.hide();
        }

        $scope.onAttchemnt = function () {
            var d = new Date();
            var uniqueName = "audio" + d.getTime() + ".pcm";
            var message = "Voice Message";
            datetime = formatAMPM();
            createTime = epocTime();
            var messageJson = '{"message":"' + message + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"' + uniqueName + '","chattime":"' + datetime + '","type":"audio","createTime":"' + createTime + '"}';
            var messageString = JSON.parse(messageJson);
            //mOpenGalleryPlateform("msg",uniqueName,"audio/pcm","callFromActivity",messageJson);
            getGalleryOpen("msg", uniqueName, "audio/pcm", "callFromActivity", messageJson);
        }

        $scope.deleteItem = function (index) {
            $scope.casedata.splice(index, 1);
            var message = "";
            for (var i = 0; i < $scope.casedata.length; i++) {
                message += ", " + $scope.casedata[i].quantity + "- " + $scope.casedata[i].itemname;
            }
            var datetime = getDateTime();
            var dt = Date.parse(new Date());
            var maxTime = 0;
            if (message.substring(1) != "") {
                var messagedata = "Your order for " + message.substring(1) + " is being processed.";
                var jsonObjString = JSON.stringify($scope.casedata);
                var name = "V Resorts";
                var datetime = formatAMPM();
                var orderstatus = 0;
                var messageJson = '{"message":"' + messagedata + '" , "messageFrom":"' + name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + datetime + '","placeorder":' + jsonObjString + ',"type":"msg","orderduration":' + maxTime + ',"orderdatetime":"' + dt + '"}';
                //openDbApiJs("postdata","callFromActivity","msg",messageJson,"data");
                setData("callFromActivity", "msg", messageJson, "data");
                var messageJsonUpdate = '{"orderitem":' + jsonObjString + ',"orderduration":' + maxTime + ',"orderdatetime":"' + dt + '","orderstatus":"' + orderstatus + '"}';
                // updateCaseDataJs(messageJsonUpdate);
                setCaseUpdate(messageJsonUpdate);
            } else {
                //doMessageToast("call");
                var messagedata = "Your order is being canceled.";
                var jsonObjString = JSON.stringify($scope.casedata);
                var name = "V Resorts";
                var datetime = formatAMPM();
                var orderstatus = 0;
                var messageJson = '{"message":"' + messagedata + '" , "messageFrom":"' + name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + datetime + '","placeorder":' + jsonObjString + ',"type":"msg","orderduration":' + maxTime + ',"orderdatetime":"' + dt + '"}';
                //openDbApiJs("postdata","callFromActivity","msg",messageJson,"data");
                setData("callFromActivity", "msg", messageJson, "data");
                var messageJsonUpdate = '{"orderitem":' + jsonObjString + ',"orderduration":' + maxTime + ',"orderdatetime":"' + dt + '","orderstatus":"' + orderstatus + '"}';
                // updateCaseDataJs(messageJsonUpdate);
                setCaseUpdate(messageJsonUpdate);
                var caseid = document.getElementById("caseid")
                    .value;
                setCancelOrder(caseid);
                //setCaseUpdate(null);
            }


        }

        $scope.setOrder = function (index, qty) {


            if (qty > 0) {
                $scope.casedata[index].quantity = qty;
            } else {
                $scope.casedata.splice(index, 1);
            }
            var message = "";
            for (var i = 0; i < $scope.casedata.length; i++) {
                message += ", " + $scope.casedata[i].quantity + "- " + $scope.casedata[i].itemname;
            }
            //doMessageToast(qty); doMessageToast("length"+$scope.casedata.length); exit;
            if ($scope.casedata.length == 0 && qty == 0) {
                var messagedata = "Your order is being canceled.";
                var jsonObjString = JSON.stringify($scope.casedata);
                var name = "V Resorts";
                var datetime = formatAMPM();
                var orderstatus = 0;
                var dt = Date.parse(new Date());
                var maxTime = 0;
                var messageJson = '{"message":"' + messagedata + '" , "messageFrom":"' + name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + datetime + '","placeorder":' + jsonObjString + ',"type":"msg","orderduration":' + maxTime + ',"orderdatetime":"' + dt + '"}';
                //openDbApiJs("postdata","callFromActivity","msg",messageJson,"data");
                setData("callFromActivity", "msg", messageJson, "data");
                var messageJsonUpdate = '{"orderitem":' + jsonObjString + ',"orderduration":' + maxTime + ',"orderdatetime":"' + dt + '","orderstatus":"' + orderstatus + '"}';
                // updateCaseDataJs(messageJsonUpdate);
                setCaseUpdate(messageJsonUpdate);
                var caseid = document.getElementById("caseid")
                    .value;
                setCancelOrder(caseid);
                //setCaseUpdate(null);
                $scope.popClose();

            } else {
                var datetime = getDateTime();
                var dt = Date.parse(new Date());
                var maxTime = 0;
                var messagedata = "Your order for " + message.substring(1) + " is being processed.";
                var jsonObjString = JSON.stringify($scope.casedata);
                var name = "V Resorts";
                var datetime = formatAMPM();
                var orderstatus = 0;
                /* var messageJson='{"message":"'+messagedata+'" , "messageFrom":"'+name+'","mobile":"'+$scope.mobile+'","filename":"test.me","chattime":"'+datetime+'","placeorder":'+jsonObjString+',"type":"msg","orderduration":'+maxTime+',"orderdatetime":"'+dt+'"}';
                 
                 setData("callFromActivity","msg",messageJson,"data");
                 var messageJsonUpdate='{"orderitem":'+jsonObjString+',"orderduration":'+maxTime+',"orderdatetime":"'+dt+'","orderstatus":"'+orderstatus+'"}';
                 setCaseUpdate(messageJsonUpdate); */

                var getdata = getObjectData(localStorage.getItem("caseid"));
                var caseData = JSON.parse(getdata);
                var filter = "case";
                var data = '{"casename":"' + caseData[0].casename + '","caseowner":"' + caseData[0].casename + '","mobile":"' + caseData[0].mobile + '","createtime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '","status":"1","orderitem":' + jsonObjString + ',"orderstatus":' + orderstatus + ',"orderduration":' + maxTime + ',"orderdatetime":"' + dt + '"}';
                var updata = updateObjectData(localStorage.getItem("caseid"), data);

                var messageJson = '{"message":"' + messagedata + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"test.me","chattime":"' + datetime + '","type":"msg","createTime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '" }';
                var ret = setObjectData(messageJson, localStorage.getItem("caseid"), String(localStorage.getItem("receivers")));

                $scope.popClose();
            }


        }

        $scope.showPopup = function (index) {
            var myPopup = $ionicPopup.show({
                template: '<div>Select Quantity</div><div class="number" ng-click="setOrder(\'' + index + '\',0)">0</div><div class="number" ng-click="setOrder(\'' + index + '\',1)">1</div><div class="number" ng-click="setOrder(\'' + index + '\',2)">2</div><div class="number" ng-click="setOrder(\'' + index + '\',3)">3</div><div class="four-ten"><div class="num" ng-click="setOrder(\'' + index + '\',4)">4</div><div class="num" ng-click="setOrder(\'' + index + '\',5)">5</div><div class="num" ng-click="setOrder(\'' + index + '\',6)">6</div><div class="num" ng-click="setOrder(\'' + index + '\',7)">7</div><div class="num" ng-click="setOrder(\'' + index + '\',8)">8</div><div class="num" ng-click="setOrder(\'' + index + '\',9)">9</div><div>',
                scope: $scope,
            })
            $scope.popClose = function () {
                myPopup.close();
            }
        }

        // .fromTemplateUrl() method
        $ionicPopover.fromTemplateUrl('popover.html', {
                scope: $scope,
                animation: $scope.animation
            })
            .then(function (popover) {
                $scope.popover = popover;
            });

        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };

        $scope.closePopover = function () {
            $scope.popover.hide();
        }

        $ionicModal.fromTemplateUrl('modal.html', {
                scope: $scope
            })
            .then(function (modal) {
                $scope.modal = modal;
            });
    }
])


app.controller("tagCtrl", ['$rootScope', "$scope", "$ionicPopover", "$ionicModal", "$stateParams", "$q", "$location", "$window", '$timeout', '$ionicScrollDelegate', '$ionicPopup', 'menuShare',
    function ($rootScope, $scope, $ionicPopover, $ionicModal, $stateParams, $q, $location, $window, $timeout, $ionicScrollDelegate, $ionicPopup, menuShare) {
        $scope.additems = [];
        $scope.platform = ionic.Platform.platform();

        $timeout(function () {
            hideQviki();
            /* var caseid = document.getElementById("caseid").value;
             var data = getCaseDetailsOfHall("0","1");
             var activeOrders = JSON.parse(data);
             for (var j = 0; j < activeOrders.length; j++){
               if(activeOrders[j].caseid == caseid){
                 caseDataObj = activeOrders[j].data;
               }
             }*/

            var getdata = getObjectData(localStorage.getItem("caseid"));

            var caseData = JSON.parse(getdata);
            if (caseData[0].orderitem != undefined) {
                doMessageToast("sadsa");
                $scope.additems = caseData[0].orderitem;
            }


        }, 100);

        $scope.showPopup = function (itemid, itemname, itemprice, duration) {
            $scope.data = {};
            var myPopup = $ionicPopup.show({
                template: '<div>Select Quantity</div><div class="number" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',0)">0</div><div class="number" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',1)">1</div><div class="number" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',2)">2</div><div class="number" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',3)">3</div><div class="four-ten"><div class="num" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',4)">4</div><div class="num" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',5)">5</div><div class="num" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',6)">6</div><div class="num" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',7)">7</div><div class="num" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',8)">8</div><div class="num" ng-click="setQuantity(\'' + itemid + '\',\'' + itemname + '\',\'' + itemprice + '\',\'' + duration + '\',9)">9</div><div>',
                scope: $scope,
            })
            $scope.popClose = function () {
                myPopup.close();
            }
        }
        $scope.setQuantity = function (itemid, itemname, itemprice, duration, qty) {
            $("#" + itemid)
                .val(qty);
            $scope.popClose();
            $scope.add(itemid, itemname, itemprice, duration, qty);

        }

        $scope.add = function (itemid, itemname, itemprice, duration, qty) {
            //doMessageToast("itemid"+itemid); 
            if ($scope.additems.length > 0) {
                var isExist = 0;
                for (var i = 0; i < $scope.additems.length; i++) {
                    // doMessageToast($scope.additems[i].itemid);
                    if ($scope.additems[i].itemid == itemid) {

                        $scope.additems.splice(i, 1);
                        //doMessageToast("match");
                        if (qty > 0) {
                            $scope.additems.push({
                                itemid: itemid,
                                itemname: itemname,
                                itemprice: itemprice,
                                duration: duration,
                                quantity: qty
                            });
                        }
                        isExist = 1;
                    }
                }
                if (isExist == 0) {
                    //doMessageToast("No match");
                    $scope.additems.push({
                        itemid: itemid,
                        itemname: itemname,
                        itemprice: itemprice,
                        duration: duration,
                        quantity: qty
                    });
                }
            } else {
                if (qty > 0) {
                    $scope.additems.push({
                        itemid: itemid,
                        itemname: itemname,
                        itemprice: itemprice,
                        duration: duration,
                        quantity: qty
                    });
                }
            }

            var totalprice = 0;
            for (var i = 0; i < $scope.additems.length; i++) {
                if (totalprice == 0) {
                    totalprice = $scope.additems[i].itemprice * $scope.additems[i].quantity;
                } else {
                    totalprice = totalprice + ($scope.additems[i].itemprice * $scope.additems[i].quantity);
                }

            }
            $("#spanTotalAmount")
                .html(totalprice);
            totalprice = 0;
            // doMessageToast($scope.additems[0].quantity);
            //doMessageToast($scope.additems[1].quantity);
        }
        $scope.isOrderExist = function (itemid) {
            // showToastPlateform(itemid);
            var qty = null;
            for (var i = 0; i < $scope.additems.length; i++) {
                if (itemid == $scope.additems[i].itemid) {
                    qty = $scope.additems[i].quantity;

                }
            }
            return qty;
        }

        $scope.submitTag = function () {

            var message = "";
            var datetime = getDateTime();
            var dt = Date.parse(new Date());
            var maxTime = 0;
            //showToastPlateform(JSON.stringify($scope.additems));
            var dd = getUserDetail();
            userObjectJS = JSON.parse(dd);
            mobile = userObjectJS[0].mobile;
            name = userObjectJS[0].firstName;


            for (var i = 0; i < $scope.additems.length; i++) {
                message += ", " + $scope.additems[i].quantity + "- " + $scope.additems[i].itemname;
            }


            if (message.substring(1) != '') {

                var messagedata = "Your order for " + message.substring(1) + " is being processed.";
                var jsonObjString = JSON.stringify($scope.additems);
                var name = "V Resorts";
                var datetime = formatAMPM();
                var orderstatus = 0;
                // doMessageToast(localStorage.getItem("caseid")); localStorage.getItem("receivers");
                var getdata = getObjectData(localStorage.getItem("caseid"));
                var caseData = JSON.parse(getdata);
                var filter = "case";
                var data = '{"casename":"' + caseData[0].casename + '","caseowner":"' + caseData[0].casename + '","mobile":"' + caseData[0].mobile + '","createtime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '","status":"1","orderitem":' + jsonObjString + ',"orderstatus":' + orderstatus + ',"orderduration":' + maxTime + ',"orderdatetime":"' + dt + '"}';
                var updata = updateObjectData(localStorage.getItem("caseid"), data);

                var messageJson = '{"message":"' + messagedata + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"test.me","chattime":"' + datetime + '","type":"msg","createTime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '" }';
                var ret = setObjectData(messageJson, localStorage.getItem("caseid"), String(localStorage.getItem("receivers")));

                /*  var messageJson='{"message":"'+messagedata+'" , "messageFrom":"'+name+'","mobile":"'+mobile+'","filename":"test.me","chattime":"'+datetime+'","placeorder":'+jsonObjString+',"type":"msg","orderduration":'+maxTime+',"orderdatetime":"'+dt+'"}';
                  setData("callFromActivity","msg",messageJson,"data");
                  var roomClient = ''; var mobileClient = ''; var nameClient = '';
                  var messageJsonUpdate='{"orderitem":'+jsonObjString+',"orderduration":'+maxTime+',"orderdatetime":"'+dt+'","orderstatus":"'+orderstatus+'"}';
                  menuShare.sendData(messageJsonUpdate);
                  setCaseUpdate(messageJsonUpdate);*/
                document.location.href = "#/chatmng";
            } else {
                doMessageToast("Please select at least one Tag.");
            }

        }

        $scope.deleteTag = function (index, itemName, itemid) {
            $scope.additems.splice(index, 1);
            $("#" + itemid)
                .val(0);
            var totalprice = 0;
            for (var i = 0; i < $scope.additems.length; i++) {
                if (totalprice == 0) {
                    totalprice = $scope.additems[i].itemprice * $scope.additems[i].quantity;
                } else {
                    totalprice = totalprice + ($scope.additems[i].itemprice * $scope.additems[i].quantity);
                }
            }
            $("#spanTotalAmount")
                .html(totalprice);
            totalprice = 0;
            doMessageToast("remove " + itemName + " successfully.");
        }

        //$scope.name = UserService.name;
        $scope.focusedHide = true;
        $scope.chatmsg = '';
        $scope.name = '';
        $scope.mobile = '';

        $timeout(function () {
            //var tagObject= $("#tagData").val();
            var tagObject = getData("item", null, "data");
            //doMessageToast("tag"+tagObject);
            $scope.items = JSON.parse(tagObject);
        }, 100);


        $scope.goBack = function () {
            doPageBack();
        }
        $scope.goHome = function () {
            doPageHome();
        }

        $scope.touchStart = function () {
            document.getElementById("search")
                .blur();
        }

        $scope.clearSearch = function () {
            $scope.search = '';
        }

        $scope.data = [{
                title: "Bad"
            },
            {
                title: "Good"
            },
            {
                title: "Great"
            },
            {
                title: "Cool"
            },
            {
                title: "Excellent"
            },
            {
                title: "Awesome"
            },
            {
                title: "Horrible"
            },
        ]

        $scope.statusCheck = function (tag) {
            //showToastPlateform("tagData"+$scope.myCheck); 
            var returnVal = false;
            for (var i = 0; i < tagData.length; i++) {
                if (tagData[i].tagID == tag) {
                    if (tagData[i].isSelected == "1") {
                        returnVal = true;
                    }
                }
            }
            return returnVal;
        }
        $scope.statusShow = function (tag) {
            //showToastPlateform("tagData"+$scope.myCheck); 
            var returnVal = false;
            for (var i = 0; i < tagData.length; i++) {
                if (tagData[i].tagID == tag) {
                    if (tagData[i].isSelected == "1") {
                        returnVal = true;
                    }
                }
            }
            return returnVal;
        }
    }
])

app.controller("headerCtrl", ['$rootScope', "$scope", "$ionicPopover", "$stateParams", "$q", "$location", "$window", '$timeout',
    function ($rootScope, $scope, $ionicPopover, $stateParams, $q, $location, $window, $timeout) {

        $scope.goHome = function () {
            goHomeJs();
        }
    }
])


app.factory('menuShare', function ($rootScope) {
        var service = {};
        service.data = false;
        service.sendData = function (data) {
            this.data = data;

            $rootScope.$broadcast('data_shared');
        };
        service.getData = function () {
            return this.data;
        };

        return service;
    })
    .directive('myTouchstart', [function () {
        return function (scope, element, attr) {

            element.on('touchstart', function (event) {
                scope.$apply(function () {
                    scope.$eval(attr.myTouchstart);
                });
            });
        };
    }])

    .directive('myTouchend', [function () {
        return function (scope, element, attr) {

            element.on('touchend', function (event) {
                scope.$apply(function () {
                    scope.$eval(attr.myTouchend);
                });
            });
        };
    }])

    .directive('fallbackSrc', function () {
        var fallbackSrc = {
            link: function postLink(scope, iElement, iAttrs) {
                iElement.bind('error', function () {
                    angular.element(this)
                        .attr("src", iAttrs.fallbackSrc);
                });
            }
        }
        return fallbackSrc;
    })

    .filter('searchItems', function () {
        return function (items, query) {
            var filtered = [];
            var letterMatch = new RegExp(query, 'i');
            //var letterMatch = text.replace(new RegExp(query, 'i'),'<span class="highlighted">$1</span>');
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (query) {
                    if (letterMatch.test(item.itemname.substring(0, query.length))) {
                        filtered.push(item);
                    }
                } else {
                    filtered.push(item);
                }
            }
            return filtered;
        };
    })



    .filter('highlight', function ($sce) {
        return function (text, phrase) {
            if (phrase) text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
                '<span class="highlighted">$1</span>')

            return $sce.trustAsHtml(text)
        }
    });


function hello(id, isComplete) {
    document.getElementById("caseid")
        .value = id;
    document.getElementById("isComplete")
        .value = isComplete;

}

function getCaseId() {
    var id = document.getElementById("caseid")
        .value;
    var data = getCaseAll(id);
    callFromActivity(data);
}

function callFromActivity(data) {
    chatView(data);
}

function deltaChanges(data) {
    chatView(data);
}

function chatView(data) {

    var dd = getUserDetail();
    userObjectJS = JSON.parse(dd);
    mobile = userObjectJS[0].mobile;
    name = userObjectJS[0].firstName;
    var ismanager = userObjectJS[0].ismanager;
    var logo = '';
    var obj = JSON.parse(data);
    var logo = 'user.jpg';
    var logov = 'muser.jpg';
    var objLenght = parseInt(obj.length) - 1;
    var timeLastOXM = findLastOXMTime(obj[objLenght].data.createTime);
    for (var i = 0; i < obj.length; i++) {

        var visitorData = getVisitorInfo(obj[i].data.mobile);
        if (visitorData == "null") {
            var logo = "user.jpg";
            logov = "muser.jpg";
        } else {
            var visitorObj = JSON.parse(visitorData);
            var logo = visitorObj.profilepic;
            var logov = visitorObj.profilepic;
            if (logo == '' || logo == null) {
                var logo = "user.jpg";
                logov = "muser.jpg";
            }
        }

        var logo_id = Math.random();

        if (mobile == obj[i].data.mobile) {
            if (obj[i].data.type == 'msg') {
                $("#chat_list")
                    .append('<div class="right-chet-wrepper" ><div class="chat-box"><div class="chat"> ' + obj[i].data.message + ' </div></div><div class="triangle-right"></div><div class="user-img" id="logo' + logo_id + '"><img src="images/' + logo + '"  class="user-logo-right" onerror="defailtLogo(' + logo_id + ');" /></div><div class="date-box"> ' + obj[i].data.chattime + ' <i class="icon d-f-a-0-ic-andr-tick-done"></i></div></div>');
            }

            if (obj[i].data.type == 'feed') {
                if (obj[i].data.feedbacktext == undefined) {
                    obj[i].data.feedbacktext = '';
                }

                if (timeLastOXM > 30) {

                    $("#chat_list")
                        .append('<div class="feedback-wrepper" style="height:auto;"><div class="text"> Feedback Pending </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field " id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="feedback" ><input type="button"  value="SEND FEEDBACK REMINDER" class="submit submit-change-color" onclick="sendfeedbackreminder();" style="margin-top: -5px; margin-bottom: 10px; width: 220px;"></div></div><div style="clear:both;"></div><br/><br/><br/><br/>');
                } else {
                    $("#chat_list")
                        .append('<div class="feedback-wrepper" style="height:100px;"><div class="text"> Feedback Pending </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field " id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text"> ' + obj[i].data.feedbacktext + ' </div></div><div style="clear:both;"></div><br/><br/><br/><br/>');
                }
            }
        } else {
            if (obj[i].data.type == 'msg') {
                $("#chat_list")
                    .append('<div class="left-chet-wrepper"><div class="user-img" id="logo' + logo_id + '"><img src="images/' + logov + '" class="user-logo-left"  onerror="defailtLogo(' + logo_id + ');" /></div><div class="triangle-left"></div><div class="chat-box">' + obj[i].data.message + '</div><div class="date-box"> ' + obj[i].data.chattime + ' <i class="icon d-f-i-1-ic-andr-double-tick-done"></i></div></div>');
            }
            if (obj[i].data.type == 'feed') {
                if (obj[i].data.feedbackstatus == 1) {
                    if (obj[i].data.feedbacktext != "") {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box" style="margin-bottom:10px;"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }


                }
                if (obj[i].data.feedbackstatus == 2) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box" style="margin-bottom:10px;"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
                if (obj[i].data.feedbackstatus == 3) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box" ><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box" style="margin-bottom:10px;"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
                if (obj[i].data.feedbackstatus == 4) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box" style="margin-bottom:10px;"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
                if (obj[i].data.feedbackstatus == 5) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#" + obj[i].uid)
                            .remove();
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box" style="margin-bottom:10px;"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
            }
        }
    }
}


function defailtLogo(image, ismanager) {
    if (ismanager == 1) {
        image.src = "images/muser.jpg";
    } else {
        image.src = "images/user.jpg";
    }
}

function onAttchemntAudio() {
    var d = new Date();
    var uniqueName = "audio" + d.getTime() + ".pcm";
    var message = "Voice Message";
    datetime = formatAMPM();
    createTime = epocTime();
    var messageJson = '{"message":"' + message + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"' + uniqueName + '","chattime":"' + datetime + '","type":"audio","createTime":"' + createTime + '"}';
    var messageString = JSON.parse(messageJson);
    mOpenGalleryPlateform("msg", uniqueName, "audio/pcm", "callFromActivity", messageJson);

}

function onAttchemnt() {
    var d = new Date();
    var uniqueName = "image" + d.getTime() + ".jpg";
    var message = "Photo";
    datetime = formatAMPM();
    createTime = epocTime();
    var messageJson = '{"message":"' + message + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"' + uniqueName + '","chattime":"' + datetime + '","type":"image","createTime":"' + createTime + '"}';
    var messageString = JSON.parse(messageJson);
    getGalleryOpen("msg", uniqueName, "image/jpeg", "callFromActivity", messageJson);
}

function iOSContextCreated() {


}



function findLastOXMTime(dateold) {
    var returnDate = "";            
    var oldDate = new Date(parseInt(dateold));            
    var currentDate = new Date();
    var diff = currentDate.getTime() - oldDate.getTime();            
    var seconds = diff / 1000;            
    var minutes = seconds / 60;         
    return parseInt(minutes);
}

function sendfeedbackreminder() {

    var dd = getUserDetail();
    userObjectJS = JSON.parse(dd);
    mobile = userObjectJS[0].mobile;
    name = userObjectJS[0].firstName;
    var message = "Request you to kindly rate your experience to complete our order";
    var oxmData = '{"message":"' + message + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"test.me","chattime":"' + formatAMPM() + '","type":"msg","createTime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '" }';

    var ret = setObjectData(oxmData, localStorage.getItem("caseid"), localStorage.getItem("receivers"));

}